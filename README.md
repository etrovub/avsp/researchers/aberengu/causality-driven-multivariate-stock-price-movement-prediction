# Causality driven multivariate stock price movement prediction

This repository contains the data and code corresponding to the forecasting models presented in the paper "Causality 
driven multivariate stock price movement prediction". The focus of this research is to investigate the relationship 
between global stock markets and the sentiments extracted from financial news to improve the accuracy of stock forecasting.

## Abstract
Our study aims to investigate the interdependence between international stock markets and sentiments from financial news 
in stock forecasting. We adopt the Temporal Fusion Transformers (TFT) [2] to incorporate intra and inter-market correlations 
and the interaction between the information flow, i.e. causality, of financial news sentiment and the dynamics of the 
stock market. The current study distinguishes itself from existing research by adopting Dynamic Transfer Entropy (DTE) [1] 
to establish an accurate information flow propagation between stock and sentiments. DTE has the advantage of providing 
time series that mine information flow propagation paths between certain parts of the time series, highlighting marginal 
events such as spikes or sudden jumps, which are crucial in financial time series. The proposed methodological approach 
involves the following elements: a FinBERT-based textual analysis of financial news articles to extract sentiment time 
series, the use of the Transfer Entropy and corresponding heat maps to analyze the net information flows, the calculation 
of the DTE time series, which are considered as co-occurring covariates of stock Price, and TFT-based stock forecasting. 
The Dow Jones Industrial Average index of 13 countries, along with daily financial news data obtained through the 
New York Times API, are used to demonstrate the validity and superiority of the proposed DTE-based causality method along 
with TFT for accurate stock Price and Return forecasting compared to state-of-the-art time series forecasting methods.

## Methodology
The proposed methodological approach involves the following stages:
- **FinBERT-based Textual Analysis**: Analyzing financial news articles to extract sentiment time series.
- **Transfer Entropy and Heat Maps**: Analyzing the information flows.
- **Dynamic Transfer Entropy (DTE) Time Series**: These are considered as co-occurring covariates of stock price/return. 
- **TFT-based Stock Forecasting**: Utilizing Temporal Fusion Transformer for accurate stock price and return forecasting.

This repository contains the code corresponding to the forecasting. 

## Data and Implementation
The study uses the Dow Jones Industrial Average index of 13 countries, along with daily financial news data 
obtained through the New York Times API. This combination demonstrates the validity and superiority of the proposed 
DTE-based causality method along with TFT for accurate stock price and return forecasting compared to state-of-the-art 
time series forecasting methods. Data used can be found in 
[Data](https://gitlab.com/aberenguer/causality-driven-multivariate-stock-price-movement-prediction/-/tree/main/Data) 
and the experiment using state-of-the-art time series forecasting methods: TFT [2], Transformers [3], DeepAR [4], DSS [5], and LSTM [6] are 
under folder [Stock_movement_forecasting](https://gitlab.com/aberenguer/causality-driven-multivariate-stock-price-movement-prediction/-/tree/main/Stock_movement_forecasting). 
One major advantage of using TFT is its interpretability. Additionally, we provide the code to interpret TFT results under 
[Stock_movement_forecasting/TFT_interpretability](https://gitlab.com/aberenguer/causality-driven-multivariate-stock-price-movement-prediction/-/tree/main/Stock_movement_forecasting/TFT_interpretability)

## GluonTS and PyTorch Forecasting
This repository relies heavily on the implementations of forecasting methods available on 
[GluonTS](https://ts.gluon.ai/stable/index.html) and 
[PyTorch Forecasting](https://pytorch-forecasting.readthedocs.io/en/latest/index.html).


### References

[1]-[Information flow analysis between EPU and other financial time series](https://www.mdpi.com/1099-4300/22/6/683)

[2]-[Temporal Fusion Transformers for interpretable multi-horizon time series forecasting (TFT)](https://www.sciencedirect.com/science/article/pii/S0169207021000637?via%3Dihub)

[3]-[Attention is all you need](https://proceedings.neurips.cc/paper_files/paper/2017/hash/3f5ee243547dee91fbd053c1c4a845aa-Abstract.html)

[4]-[DeepAR: Probabilistic forecasting with autoregressive recurrent networks (DeepAR)](https://www.sciencedirect.com/science/article/pii/S0169207019301888?via%3Dihub)

[5]-[Deep State Space Models for Time Series Forecasting (DSS)](https://papers.nips.cc/paper/2018/hash/5cf68969fb67aa6082363a6d4e6468e2-Abstract.html)

[6]-[Long-Sort Term Memory (LSTM)](https://ieeexplore.ieee.org/abstract/document/6795963)


**Note**: The references mentioned are cited in the paper.





#!/usr/bin/env python
# coding: utf-8
import os
import utils
import argparse
import sys
def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')
##########################
### SETTINGS
##########################
parser = argparse.ArgumentParser(description='Stock Forecasting')
##########################
### Data
##########################
parser.add_argument('--input_data', metavar='input_data', type=str,
                    default=os.path.join(os.path.join(os.path.dirname(os.getcwd()),'Data'),'price_return_index_te'),#Path to data #E.g, os.path.join('..', '..','Data')#,
                    help='Path to the root folder with the training data')
parser.add_argument('--root_path_to_save_results', metavar='root_path_to_save_results', type=str,
                    default=os.path.join(os.path.dirname(os.getcwd()), 'Results'),#Path to results#E.g,os.path.join('..', '..', 'Resutls')#
                    help='Path to the root folder to save the results')
# ##########################
# ### Data preprocessing
# ##########################
parser.add_argument('--do_normalize', metavar='do_normalize', type=str2bool,
                    default=True,
                    help='Define if normalize or not')

parser.add_argument('--stock_target', metavar='stock_target', type=str,
                    default='Price', choices=['Price', 'Return'],
                    help='Define the stock target to predic or not')
##########################
### Exp Settings
##########################
parser.add_argument('--model', type=str, choices=['TFT', 'DSS', 'DeepAR', 'LSTM','Transformer'],
                    help='The forecasting model to use',
                    default='TFT')#So far 3 models

parser.add_argument('--forecasting_horizon', type=int,
                    help='The forecasting horizon (windows length)',
                    default=7)

parser.add_argument('--prediction_length', type=int,
                    help='The forecasting period',
                    default=90)
##########################
### Model Optimization
##########################
parser.add_argument('--n_epochs', type=int,
                    help='Number of epochs to train the models',
                    default=2)

parser.add_argument('--learning_rate', type=float,
                    help='Models learning rate',
                    default=1e-3)

parser.add_argument('--num_batches_per_epoch', type=int,
                    help='Number of batches per epoch',
                    default= 100)
def main(args):
    n_epochs = args.n_epochs
    if args.stock_target=='Price':
        exp_list = ['do_Price_per_Country', 'do_Price_and_Sentiment_per_Country', 'do_all_Covariates_per_Country',
                    'do_Price_and_TExy_per_Country', 'do_Price_and_TEyx_per_Country', 'do_Price_and_TExyyx_per_Country',
                    'do_Price_and_Sentiment_per_country_and_all_Countries', 'do_Price_per_country_and_all_Countries_Price',
                    'do_Price_per_country_and_all_Countries_Price_and_Sentiment']

        results_head = {'do_Price_per_Country': 'A',  # 'Domestic: Price',
                        'do_Price_and_Sentiment_per_Country': 'B',  # 'Domestic: Price and Sentiment Index',
                        'do_Price_and_TExy_per_Country': 'C',  # 'Domestic: Price and TE - XY',
                        'do_Price_and_TEyx_per_Country': 'D',  # 'Domestic: Price and TE - YX',
                        'do_Price_and_TExyyx_per_Country': 'E',  # 'Domestic: Price, TE - XY and TE - YX',
                        'do_all_Covariates_per_Country': 'F',
                        # 'Domestic: Price, Return, Low, High, Open, Sentiment Index, TE - XY, TE - YX',
                        'do_Price_and_Sentiment_per_country_and_all_Countries': 'G',
                        # Domestic: Price and Sentiment Index. Foreign: Price and Sentiment Index',
                        'do_Price_per_country_and_all_Countries_Price_and_Sentiment': 'H',
                        # 'Domestic: Price and Sentiment Index. Foreign: Sentiment Index',
                        'do_Price_per_country_and_all_Countries_Price': 'I'}  # 'Domestic: Price.Foreign: Price'}
        data, countries_list = utils.load_csv_price_from_folder(folder_path=args.input_data,
                                                                do_normalize=args.do_normalize)

    elif args.stock_target=='Return':
        exp_list = ['do_Return_per_Country', 'do_Return_and_Sentiment_per_Country', 'do_all_Covariates_per_Country',
                    'do_Return_and_TExy_per_Country', 'do_Return_and_TEyx_per_Country',
                    'do_Return_and_TExyyx_per_Country',
                    'do_Return_and_Sentiment_per_country_and_all_Countries',
                    'do_Return_per_country_and_all_Countries_Return',
                    'do_Return_per_country_and_all_Countries_Return_and_Sentiment']

        results_head = {'do_Return_per_Country': 'A',  # 'Domestic: Price',
                        'do_Return_and_Sentiment_per_Country': 'B',  # 'Domestic: Return and Sentiment Index',
                        'do_Return_and_TExy_per_Country': 'C',  # 'Domestic: Return and TE - XY',
                        'do_Return_and_TEyx_per_Country': 'D',  # 'Domestic: Return and TE - YX',
                        'do_Return_and_TExyyx_per_Country': 'E',  # 'Domestic: Return, TE - XY and TE - YX',
                        'do_all_Covariates_per_Country': 'F',
                        # 'Domestic: Price, Return, Low, High, Open, Sentiment Index, TE - XY, TE - YX',
                        'do_Return_and_Sentiment_per_country_and_all_Countries': 'G',
                        # Domestic: Return and Sentiment Index. Foreign: Return and Sentiment Index',
                        'do_Return_per_country_and_all_Countries_Return_and_Sentiment': 'H', # 'Domestic: Return and Sentiment Index. Foreign: Sentiment Index',
                        'do_Return_per_country_and_all_Countries_Return': 'I', # 'Domestic: Return.Foreign: Return'
                        }

        data, countries_list = utils.load_csv_return_from_folder(folder_path=args.input_data,
                                                                do_normalize=args.do_normalize)

    utils.train_evaluate_and_save_results(path_to_save_results=os.path.join(os.path.join(args.root_path_to_save_results,args.model),args.stock_target),
                                          data=data, exp_list=exp_list, countries_list=countries_list,
                                          results_head=results_head, stock_target=args.stock_target,
                                          prediction_length=args.prediction_length, forecasting_horizon=args.forecasting_horizon,
                                          model=args.model, n_epochs=args.n_epochs, learning_rate=args.learning_rate, num_batches_per_epoch=args.num_batches_per_epoch)

if __name__ == '__main__':
    argsin = sys.argv[1:]
    args = parser.parse_args(argsin)
    # Print keys and values
    print('***---***' * 10+'Experimental Settings'+'***---***' * 10)
    for key, value in args.__dict__.items():
        print( f"{key}={value}")
    main(args)
    sys.exit()
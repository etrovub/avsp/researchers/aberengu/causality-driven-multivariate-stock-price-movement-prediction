#!/bin/bash
# redirect standard output (-o) and error (-e)
#SBATCH --job-name=train_evaluate_forecasting_models
#SBATCH --output=../Results/%x-%j-%a.out
#SBATCH --error=../Results/%x-%j-%a.err


#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --time=100:00:00
#SBATCH --mem=64gb
#SBATCH --array=1-10


echo Start ArrayID: $SLURM_ARRAY_TASK_ID
# Specify the path to the config file
cd $SLURM_SUBMIT_DIR
config=forecasting_experiments_array_ID_config.csv

# Extract the model name for the current $SLURM_ARRAY_TASK_ID
model=$(awk -F "\"*,\"*" -v ArrayTaskID=$SLURM_ARRAY_TASK_ID '$1==ArrayTaskID {print $2}' $config)

# Extract the forecasting_horizon for the current $SLURM_ARRAY_TASK_ID
forecasting_horizon=$(awk -F "\"*,\"*" -v ArrayTaskID=$SLURM_ARRAY_TASK_ID '$1==ArrayTaskID {print $3}' $config)

# Extract the stock_target for the current $SLURM_ARRAY_TASK_ID
stock_target=$(awk -F "\"*,\"*" -v ArrayTaskID=$SLURM_ARRAY_TASK_ID '$1==ArrayTaskID {print $4}' $config)

# Extract the stock_target for the current $SLURM_ARRAY_TASK_ID
n_epochs=$(awk -F "\"*,\"*" -v ArrayTaskID=$SLURM_ARRAY_TASK_ID '$1==ArrayTaskID {print $5}' $config)

python train_evaluate_models.py --model=$model --forecasting_horizon=$forecasting_horizon --n_epochs=$n_epochs --learning_rate=1e-3 --num_batches_per_epoch=100 --stock_target=$stock_target | tee ../Results/Logs_${model}_${stock_target}_windsize_${forecasting_horizon}.txt
echo End ArrayID: $SLURM_ARRAY_TASK_ID
import pandas as pd
import numpy as np
import json
import errno
import os
from sklearn import preprocessing
##gluonts
from gluonts.mx import TemporalFusionTransformerEstimator, DeepAREstimator, DeepStateEstimator , TransformerEstimator, Trainer
from gluonts.mx.model.canonical import CanonicalRNNEstimator
from gluonts.dataset.split import DateSplitter
from gluonts.dataset.rolling_dataset import StepStrategy, generate_rolling_dataset
from gluonts.mx import Trainer
from gluonts.evaluation import make_evaluation_predictions, Evaluator


def load_csv_price_from_folder(folder_path,do_normalize):
    csv_list = []
    countries_list = []
    for file_name in os.listdir(folder_path):
        file_path = os.path.join(folder_path, file_name)
        if os.path.isfile(file_path) and file_name.endswith('.csv'):
            df_raw = (pd.read_csv(file_path, header=0, parse_dates=True))
            ds=[{'target': df_raw["Price"].to_numpy(),
                'return': df_raw['Return'].to_numpy(), #returns a numpy array
                'open': df_raw['Open'].to_numpy(),
                'high': df_raw['High'].to_numpy(),
                'low': df_raw['Low'].to_numpy(),
                'index':df_raw['Index'].to_numpy(),
                'texy':df_raw['TE(XY)'].to_numpy(),
                'teyx': df_raw['TE(YX)'].to_numpy(),
                'zxy':df_raw['Z(XY)'].to_numpy(),
                'zyx': df_raw['Z(YX)'].to_numpy(),
                 "start": pd.Period('2020-03-02', freq='1D')
                }]
            if do_normalize:
                min_max_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
                target_values = ds[0]['target'] #returns a numpy array
                return_values = ds[0]['return']
                open_values = ds[0]['open']
                high_values = ds[0]['high']
                low_values = ds[0]['low']
                index_values = ds[0]['index']
                texy_values = ds[0]['texy']
                teyx_values = ds[0]['teyx']
                zxy_values = ds[0]['zxy']
                zyx_values = ds[0]['zyx']
                target_values_scaled = min_max_scaler.fit_transform(target_values.reshape(-1, 1))
                return_values_scaled = min_max_scaler.fit_transform(return_values.reshape(-1, 1))
                open_values_scaled = min_max_scaler.fit_transform(open_values.reshape(-1, 1))
                high_values_scaled = min_max_scaler.fit_transform(high_values.reshape(-1, 1))
                low_values_scaled = min_max_scaler.fit_transform(low_values.reshape(-1, 1))
                index_values_scaled = min_max_scaler.fit_transform(index_values.reshape(-1, 1))
                texy_values_scaled = min_max_scaler.fit_transform(texy_values.reshape(-1, 1))
                teyx_values_scaled = min_max_scaler.fit_transform(teyx_values.reshape(-1, 1))
                zxy_values_scaled = min_max_scaler.fit_transform(zxy_values.reshape(-1, 1))
                zyx_values_scaled = min_max_scaler.fit_transform(zyx_values.reshape(-1, 1))
                ds[0]['target'] = target_values_scaled.reshape(1, -1)[0]
                ds[0]['return'] = return_values_scaled.reshape(1, -1)[0]
                ds[0]['open'] = open_values_scaled.reshape(1, -1)[0]
                ds[0]['high'] = high_values_scaled.reshape(1, -1)[0]
                ds[0]['low'] = low_values_scaled.reshape(1, -1)[0]
                ds[0]['index'] = index_values_scaled.reshape(1, -1)[0]
                ds[0]['texy'] = texy_values_scaled.reshape(1, -1)[0]
                ds[0]['teyx'] = teyx_values_scaled.reshape(1, -1)[0]
                ds[0]['zxy'] = zxy_values_scaled.reshape(1, -1)[0]
                ds[0]['zyx'] = zyx_values_scaled.reshape(1, -1)[0]
            csv_list.append(ds)
            countries_list.append(os.path.basename(file_name).split(".")[0])
    return csv_list, countries_list#, scaler_list

def load_csv_return_from_folder(folder_path,do_normalize):
    csv_list = []
    countries_list = []
    for file_name in os.listdir(folder_path):
        file_path = os.path.join(folder_path, file_name)
        if os.path.isfile(file_path) and file_name.endswith('.csv'):
            df_raw = (pd.read_csv(file_path, header=0, parse_dates=True))
            ds = [{'target': df_raw["Return"].to_numpy(),
                   'price': df_raw['Price'].to_numpy(),  # returns a numpy array
                   'open': df_raw['Open'].to_numpy(),
                   'high': df_raw['High'].to_numpy(),
                   'low': df_raw['Low'].to_numpy(),
                   'index': df_raw['Index'].to_numpy(),
                   'texy': df_raw['TE(XY)'].to_numpy(),
                   'teyx': df_raw['TE(YX)'].to_numpy(),
                   'zxy': df_raw['Z(XY)'].to_numpy(),
                   'zyx': df_raw['Z(YX)'].to_numpy(),
                   "start": pd.Period('2020-03-02', freq='1D')
                   }]
            if do_normalize:
                min_max_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
                target_values = ds[0]['target']  # returns a numpy array
                price_values = ds[0]['price']
                open_values = ds[0]['open']
                high_values = ds[0]['high']
                low_values = ds[0]['low']
                index_values = ds[0]['index']
                texy_values = ds[0]['texy']
                teyx_values = ds[0]['teyx']
                zxy_values = ds[0]['zxy']
                zyx_values = ds[0]['zyx']
                target_values_scaled = min_max_scaler.fit_transform(target_values.reshape(-1, 1))
                price_values_scaled = min_max_scaler.fit_transform(price_values.reshape(-1, 1))
                open_values_scaled = min_max_scaler.fit_transform(open_values.reshape(-1, 1))
                high_values_scaled = min_max_scaler.fit_transform(high_values.reshape(-1, 1))
                low_values_scaled = min_max_scaler.fit_transform(low_values.reshape(-1, 1))
                index_values_scaled = min_max_scaler.fit_transform(index_values.reshape(-1, 1))
                texy_values_scaled = min_max_scaler.fit_transform(texy_values.reshape(-1, 1))
                teyx_values_scaled = min_max_scaler.fit_transform(teyx_values.reshape(-1, 1))
                zxy_values_scaled = min_max_scaler.fit_transform(zxy_values.reshape(-1, 1))
                zyx_values_scaled = min_max_scaler.fit_transform(zyx_values.reshape(-1, 1))
                ds[0]['target'] = target_values_scaled.reshape(1, -1)[0]
                ds[0]['price'] = price_values_scaled.reshape(1, -1)[0]
                ds[0]['open'] = open_values_scaled.reshape(1, -1)[0]
                ds[0]['high'] = high_values_scaled.reshape(1, -1)[0]
                ds[0]['low'] = low_values_scaled.reshape(1, -1)[0]
                ds[0]['index'] = index_values_scaled.reshape(1, -1)[0]
                ds[0]['texy'] = texy_values_scaled.reshape(1, -1)[0]
                ds[0]['teyx'] = teyx_values_scaled.reshape(1, -1)[0]
                ds[0]['zxy'] = zxy_values_scaled.reshape(1, -1)[0]
                ds[0]['zyx'] = zyx_values_scaled.reshape(1, -1)[0]
            csv_list.append(ds)
            countries_list.append(os.path.basename(file_name).split(".")[0])
    return csv_list, countries_list


def exp_setup_price(data_list, exp_list,expriment_seeting='do_Price_per_Country'):
    data = []
    if expriment_seeting == 'do_Price_per_Country':
        print('***' * 10 + 'Experiments Country Price ' + '***' * 10)
        for ds in data_list:
            ds_copy = ds[0].copy()
            keys = ['return', 'open', 'high', 'low', 'index', 'texy', 'teyx', 'zxy', 'zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting == 'do_Price_and_Sentiment_per_Country':
        print('***' * 10 + 'Experiments Country Price and Sentiment' + '***' * 10)
        for ds in data_list:
            ds_copy = ds[0].copy()
            keys = ['return', 'open', 'high', 'low', 'texy', 'teyx', 'zxy', 'zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting == 'do_all_Covariates_per_Country':
        print('***' * 10 + 'Experiments Country All Covariates' + '***' * 10)
        for ds in data_list:
            ds_copy = ds[0].copy()
            data.append([ds_copy])
        return data
    elif expriment_seeting == 'do_Price_and_TExy_per_Country':
        print('***' * 10 + 'Experiments Country Price and TE X->Y' + '***' * 10)
        for ds in data_list:
            ds_copy = ds[0].copy()
            keys = ['return', 'open', 'high', 'low', 'index', 'teyx', 'zxy', 'zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting == 'do_Price_and_TEyx_per_Country':
        print('***' * 10 + 'Experiments Country Price and TE Y->X' + '***' * 10)
        for ds in data_list:
            ds_copy = ds[0].copy()
            keys = ['return', 'open', 'high', 'low', 'index', 'texy', 'zxy', 'zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting == 'do_Price_and_TExyyx_per_Country':
        print('***' * 10 + 'Experiments Country Price and TE XY->YX' + '***' * 10)
        for ds in data_list:
            ds_copy = ds[0].copy()
            keys = ['return', 'open', 'high', 'low', 'index', 'zxy', 'zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting == 'do_Price_and_Sentiment_per_country_and_all_Countries':
        print(
            '***' * 10 + 'Experiments Country Price and Sentiment plus All Other Countries Prices and Sentiments ' + '***' * 10)
        for idx_country, ds in enumerate(data_list):
            ds_copy = ds[0].copy()
            keys = ['return', 'open', 'high', 'low', 'texy', 'teyx', 'zxy', 'zyx']

            for key in keys:
                ds_copy.pop(key, None)

            for idx_other_countries, ds_other_countries in enumerate(data_list):
                if idx_other_countries != idx_country:
                    temp_key_target = 'target_{}'.format(idx_other_countries)
                    temp_key_index = 'index_{}'.format(idx_other_countries)
                    ds_copy[temp_key_target] = ds_other_countries[0]['target']
                    ds_copy[temp_key_index] = ds_other_countries[0]['index']
            data.append([ds_copy])
        return data
    elif expriment_seeting == 'do_Price_per_country_and_all_Countries_Price':
        print('***' * 10 + 'Experiments Country Price plus All Other Countries Prices ' + '***' * 10)
        for idx_country, ds in enumerate(data_list):
            ds_copy = ds[0].copy()
            keys = ['return', 'open', 'high', 'low', 'index', 'texy', 'teyx', 'zxy', 'zyx']
            for key in keys:
                ds_copy.pop(key, None)

            for idx_other_countries, ds_other_countries in enumerate(data_list):
                if idx_other_countries != idx_country:
                    temp_key_target = 'target_{}'.format(idx_other_countries)
                    ds_copy[temp_key_target] = ds_other_countries[0]['target']
            data.append([ds_copy])
        return data
    elif expriment_seeting == 'do_Price_per_country_and_all_Countries_Price_and_Sentiment':
        print('***' * 10 + 'Experiments Country Price plus All Other Countries Prices and Sentiments ' + '***' * 10)
        for idx_country, ds in enumerate(data_list):
            ds_copy = ds[0].copy()
            keys = ['return', 'open', 'high', 'low', 'index', 'texy', 'teyx', 'zxy', 'zyx']

            for key in keys:
                ds_copy.pop(key, None)

            for idx_other_countries, ds_other_countries in enumerate(data_list):
                if idx_other_countries != idx_country:
                    temp_key_target = 'target_{}'.format(idx_other_countries)
                    temp_key_index = 'index_{}'.format(idx_other_countries)
                    ds_copy[temp_key_target] = ds_other_countries[0]['target']
                    ds_copy[temp_key_index] = ds_other_countries[0]['index']

            data.append([ds_copy])
        return data
    else:
        raise ValueError(
            "Experiment {} Not Implemented select an option from this list {}...\n".format(expriment_seeting, exp_list))


def exp_setup_return(data_list, exp_list, expriment_seeting='do_Return_per_Country'):
    data=[]
    if expriment_seeting=='do_Return_per_Country':
        print('***'*10+'Experiments Country Return '+'***'*10)
        for ds in data_list:
            ds_copy=ds[0].copy()
            keys=['price','open','high','low','index','texy','teyx','zxy','zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting=='do_Return_and_Sentiment_per_Country':
        print('***'*10+'Experiments Country Return and Sentiment'+'***'*10)
        for ds in data_list:
            ds_copy=ds[0].copy()
            keys=['price','open','high','low','texy','teyx','zxy','zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting=='do_all_Covariates_per_Country':
        print('***'*10+'Experiments Country All Covariates'+'***'*10)
        for ds in data_list:
            ds_copy=ds[0].copy()
            data.append([ds_copy])
        return data
    elif expriment_seeting=='do_Return_and_TExy_per_Country':
        print('***'*10+'Experiments Country Return and TE X->Y'+'***'*10)
        for ds in data_list:
            ds_copy=ds[0].copy()
            keys=['price','open','high','low','index','teyx','zxy','zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting=='do_Return_and_TEyx_per_Country':
        print('***'*10+'Experiments Country Return and TE Y->X'+'***'*10)
        for ds in data_list:
            ds_copy=ds[0].copy()
            keys=['price','open','high','low','index','texy','zxy','zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting=='do_Return_and_TExyyx_per_Country':
        print('***'*10+'Experiments Country Return and TE XY->YX'+'***'*10)
        for ds in data_list:
            ds_copy=ds[0].copy()
            keys=['price','open','high','low','index','zxy','zyx']
            for key in keys:
                ds_copy.pop(key, None)
            data.append([ds_copy])
        return data
    elif expriment_seeting=='do_Return_and_Sentiment_per_country_and_all_Countries':
        print('***'*10+'Experiments Country Return and Sentiment plus All Other Countries Prices and Sentiments '+'***'*10)
        for idx_country, ds in enumerate(data_list):
            ds_copy=ds[0].copy()
            keys=['price','open','high','low','texy','teyx','zxy','zyx']

            for key in keys:
                ds_copy.pop(key, None)

            for idx_other_countries, ds_other_countries in enumerate(data_list):
                if idx_other_countries!=idx_country:
                    temp_key_target='target_{}'.format(idx_other_countries)
                    temp_key_index='index_{}'.format(idx_other_countries)
                    ds_copy[temp_key_target]=ds_other_countries[0]['target']
                    ds_copy[temp_key_index]=ds_other_countries[0]['index']
            data.append([ds_copy])
        return data
    elif expriment_seeting=='do_Return_per_country_and_all_Countries_Return':
        print('***'*10+'Experiments Country Return plus All Other Countries Returns '+'***'*10)
        for idx_country, ds in enumerate(data_list):
            ds_copy=ds[0].copy()
            keys=['price','open','high','low','index','texy','teyx','zxy','zyx']
            for key in keys:
                ds_copy.pop(key, None)

            for idx_other_countries, ds_other_countries in enumerate(data_list):
                if idx_other_countries!=idx_country:
                    temp_key_target='target_{}'.format(idx_other_countries)
                    ds_copy[temp_key_target]=ds_other_countries[0]['target']
            data.append([ds_copy])
        return data
    elif expriment_seeting=='do_Return_per_country_and_all_Countries_Return_and_Sentiment':
        print('***'*10+'Experiments Country Return plus All Other Countries Returns and Sentiments '+'***'*10)
        for idx_country, ds in enumerate(data_list):
            ds_copy=ds[0].copy()
            keys=['price','open','high','low','index','texy','teyx','zxy','zyx']

            for key in keys:
                ds_copy.pop(key, None)

            for idx_other_countries, ds_other_countries in enumerate(data_list):
                if idx_other_countries!=idx_country:
                    temp_key_target='target_{}'.format(idx_other_countries)
                    temp_key_index='index_{}'.format(idx_other_countries)
                    ds_copy[temp_key_target]=ds_other_countries[0]['target']
                    ds_copy[temp_key_index]=ds_other_countries[0]['index']

            data.append([ds_copy])
        return data
    else:
        raise ValueError(
            "Experiment {} Not Implemented select an option from this list {}...\n".format(expriment_seeting, exp_list))


def train_evaluate_and_save_results(path_to_save_results,data,exp_list,countries_list,results_head,stock_target,
                                    prediction_length,forecasting_horizon,model, n_epochs,learning_rate,num_batches_per_epoch):

    df_results_RMSE = pd.DataFrame(columns=['countries'] + list(results_head.values()),
                                   index=range(0, len(countries_list) + 2))
    df_results_RMSE['countries'] = countries_list + ['mean'] + ['sd']

    df_results_AE = pd.DataFrame(columns=['countries'] + list(results_head.values()),
                                 index=range(0, len(countries_list) + 2))
    df_results_AE['countries'] = countries_list + ['mean'] + ['sd']

    df_results_MAE = pd.DataFrame(columns=['countries'] + list(results_head.values()),
                                  index=range(0, len(countries_list) + 2))
    df_results_MAE['countries'] = countries_list + ['mean'] + ['sd']

    df_results_MAPE = pd.DataFrame(columns=['countries'] + list(results_head.values()),
                                   index=range(0, len(countries_list) + 2))
    df_results_MAPE['countries'] = countries_list + ['mean'] + ['sd']

    df_results_sMAPE = pd.DataFrame(columns=['countries'] + list(results_head.values()),
                                    index=range(0, len(countries_list) + 2))
    df_results_sMAPE['countries'] = countries_list + ['mean'] + ['sd']

    ### If does not exists create the folder...
    if not os.path.exists(path_to_save_results):
        try:
            os.makedirs(path_to_save_results)
        except OSError as exception:
            if exception.errno == errno.EEXIST and os.path.isdir(path_to_save_results):
                pass
            else:
                raise ValueError(
                    "Failed to created output directory {}...\n".format(path_to_save_results))
    for exp_idx, exp in enumerate(exp_list):

        df_exp_country_results_RMSE = pd.DataFrame(columns=['countries', results_head[exp]],
                                                   index=range(0, len(countries_list) + 2))
        df_exp_country_results_RMSE['countries'] = countries_list + ['mean'] + ['sd']

        df_exp_country_results_AE = pd.DataFrame(columns=['countries', results_head[exp]],
                                                 index=range(0, len(countries_list) + 2))
        df_exp_country_results_AE['countries'] = countries_list + ['mean'] + ['sd']

        df_exp_country_results_MAE = pd.DataFrame(columns=['countries', results_head[exp]],
                                                  index=range(0, len(countries_list) + 2))
        df_exp_country_results_MAE['countries'] = countries_list + ['mean'] + ['sd']

        df_exp_country_results_MAPE = pd.DataFrame(columns=['countries', results_head[exp]],
                                                   index=range(0, len(countries_list) + 2))
        df_exp_country_results_MAPE['countries'] = countries_list + ['mean'] + ['sd']

        df_exp_country_results_sMAPE = pd.DataFrame(columns=['countries', results_head[exp]],
                                                    index=range(0, len(countries_list) + 2))
        df_exp_country_results_sMAPE['countries'] = countries_list + ['mean'] + ['sd']

        if stock_target == 'Price':
            data_to_use = exp_setup_price(data, exp_list, expriment_seeting=exp)
        elif stock_target == 'Return':
            data_to_use = exp_setup_return(data, exp_list, expriment_seeting=exp)

        for idx, country in enumerate(countries_list):
            print('***---***' * 10)
            print('Startarted training for country {}---Country {} of {}'.format(country, idx + 1, len(countries_list)))

            rolled = generate_rolling_dataset(
                dataset=data_to_use[idx],
                strategy=StepStrategy(prediction_length=7),
                start_time=pd.Period('2020-03-02', '1D'),
                end_time=pd.Period('2021-10-31', '1D')
            )
            splitter = DateSplitter(date=pd.Period('2021-08-01', freq='1D'))
            training_dataset, test_template = splitter.split(rolled)

            test_pairs = test_template.generate_instances(
                prediction_length=prediction_length,
                windows=int(prediction_length / forecasting_horizon),
            )

            entry = next(iter(test_pairs))

            ##Declare estimator
            if model == 'DSS':
                estimator = DeepStateEstimator(
                    freq='1D',
                    prediction_length=forecasting_horizon,
                    cardinality=[2],
                    use_feat_static_cat=False,
                    trainer=Trainer(ctx="cpu", epochs=n_epochs, learning_rate=learning_rate,
                                    num_batches_per_epoch=num_batches_per_epoch),
                )

            elif model == 'DeepAR':
                estimator = DeepAREstimator(
                    freq='1D',
                    prediction_length=forecasting_horizon,
                    context_length=100,
                    trainer=Trainer(ctx="cpu", epochs=n_epochs, learning_rate=learning_rate,
                                    num_batches_per_epoch=num_batches_per_epoch),
                )
            elif model == 'LSTM':
                estimator = CanonicalRNNEstimator(
                    freq='1D',
                    prediction_length=forecasting_horizon,
                    context_length=100,
                    trainer=Trainer(ctx="cpu", epochs=n_epochs, learning_rate=learning_rate,
                                    num_batches_per_epoch=num_batches_per_epoch),
                )

            elif model == 'Transformer':
                estimator = TransformerEstimator(
                    freq='1D',
                    prediction_length=forecasting_horizon,
                    context_length=100,
                    trainer=Trainer(ctx="cpu", epochs=n_epochs, learning_rate=learning_rate,
                                    num_batches_per_epoch=num_batches_per_epoch),
                )

            elif model == 'TFT':
                estimator = TemporalFusionTransformerEstimator(
                    freq='1D',
                    prediction_length=forecasting_horizon,
                    context_length=100,
                    trainer=Trainer(ctx="cpu", epochs=n_epochs, learning_rate=learning_rate,
                                    num_batches_per_epoch=num_batches_per_epoch),
                )

            ##Train estimator
            predictor = estimator.train(training_dataset)

            forecast_it, ts_it = make_evaluation_predictions(
                dataset=entry,  # test dataset
                predictor=predictor,  # predictor
                num_samples=5,  # number of sample paths we want for evaluation
            )
            ##First, we can convert these generators to lists to ease the subsequent computations.
            forecasts = list(forecast_it)
            tss = list(ts_it)
            print('*****Evaluations for country {}*****'.format(country))
            evaluator = Evaluator(quantiles=[0.1, 0.5, 0.9])

            agg_metrics, item_metrics = evaluator(tss, forecasts)
            df_results_RMSE.at[idx, results_head[exp]] = agg_metrics['RMSE']
            df_results_AE.at[idx, results_head[exp]] = agg_metrics['abs_error']
            df_results_MAE.at[idx, results_head[exp]] = agg_metrics['abs_error'] / entry[1]['target'].size
            df_results_MAPE.at[idx, results_head[exp]] = agg_metrics['MAPE']
            df_results_sMAPE.at[idx, results_head[exp]] = agg_metrics['sMAPE']

            print('Metrics for country {} experiment  {}: RMSE= {:.4f} | AE= {:.4f} | MAE= {:.4f} | MAPE= {:.4f} |  sMAPE= {:.4f} |'.format(country,results_head[exp],
                                                                                                                   df_results_RMSE.loc[
                                                                                                                       idx,
                                                                                                                       results_head[
                                                                                                                           exp]],
                                                                                                                   df_results_AE.loc[
                                                                                                                       idx,
                                                                                                                       results_head[
                                                                                                                           exp]],
                                                                                                                   df_results_MAE.loc[
                                                                                                                       idx,
                                                                                                                       results_head[
                                                                                                                           exp]],
                                                                                                                   df_results_MAPE.loc[
                                                                                                                       idx,
                                                                                                                       results_head[
                                                                                                                           exp]],
                                                                                                                   df_results_sMAPE.loc[
                                                                                                                       idx,
                                                                                                                       results_head[
                                                                                                                           exp]]))

            print(json.dumps(agg_metrics, indent=4))
            item_metrics.head()
            print('Finished training for country {}---Country {} of {}'.format(country, idx + 1, len(countries_list)))


        df_results_RMSE.at[len(countries_list), results_head[exp]] = df_results_RMSE[results_head[exp]].iloc[
                                                        0:(len(countries_list) - 1)].mean()
        df_results_RMSE.at[len(countries_list)+1, results_head[exp]] = df_results_RMSE[results_head[exp]].iloc[
                                                      0:(len(countries_list) - 1)].std()

        df_results_AE.at[len(countries_list), results_head[exp]] = df_results_AE[results_head[exp]].iloc[
                                                      0:(len(countries_list) - 1)].mean()
        df_results_AE.at[len(countries_list)+1, results_head[exp]] = df_results_AE[results_head[exp]].iloc[0:(len(countries_list) - 1)].std()


        df_results_MAE.at[len(countries_list), results_head[exp]] = df_results_MAE[results_head[exp]].iloc[
                                                       0:(len(countries_list) - 1)].mean()
        df_results_MAE.at[len(countries_list)+1, results_head[exp]] = df_results_MAE[results_head[exp]].iloc[
                                                     0:(len(countries_list) - 1)].std()

        df_results_MAPE.at[len(countries_list), results_head[exp]] = df_results_MAPE[results_head[exp]].iloc[
                                                        0:(len(countries_list) - 1)].mean()
        df_results_MAPE.at[len(countries_list)+1, results_head[exp]] = df_results_MAPE[results_head[exp]].iloc[
                                                      0:(len(countries_list) - 1)].std()

        df_results_sMAPE.at[len(countries_list), results_head[exp]] = df_results_sMAPE[results_head[exp]].iloc[
                                                                     0:(len(countries_list) - 1)].mean()
        df_results_sMAPE.at[len(countries_list) + 1, results_head[exp]] = df_results_sMAPE[results_head[exp]].iloc[
                                                                         0:(len(countries_list) - 1)].std()

        ##Save this experiment RMSE results for all countries
        df_exp_country_results_RMSE.loc[:, results_head[exp]] = df_results_RMSE.loc[:, results_head[exp]].copy()
        print('-*-' * 10 + "Saving resutls for exp {} to {} ".format(results_head[exp],
                                                                     str(os.path.join(path_to_save_results,
                                                                                      'output_exp_{}_windows_{}_RMSE.csv'.format(
                                                                                          results_head[exp], forecasting_horizon)))) + '-*-' * 10 + '\n')
        df_exp_country_results_RMSE.to_csv(
            os.path.join(path_to_save_results, 'output_exp_{}_windows_{}_RMSE.csv'.format(results_head[exp],forecasting_horizon)), index=False)

        ##Save this experiment AE results for all countries
        df_exp_country_results_AE.loc[:, results_head[exp]] = df_results_AE.loc[:, results_head[exp]].copy()
        print('-*-' * 10 + "Saving results for exp {} to {} ".format(results_head[exp],
                                                                     str(os.path.join(path_to_save_results,
                                                                                      'output_exp_{}_windows_{}_AE.csv'.format(
                                                                                          results_head[exp], forecasting_horizon)))) + '-*-' * 10 + '\n')
        df_exp_country_results_AE.to_csv(
            os.path.join(path_to_save_results, 'output_exp_{}_windows_{}_AE.csv'.format(results_head[exp],forecasting_horizon)), index=False)

        ##Save this experiment MAE results for all countries
        df_exp_country_results_MAE.loc[:, results_head[exp]] = df_results_MAE.loc[:, results_head[exp]].copy()
        print('-*-' * 10 + "Saving results for exp {} to {} ".format(results_head[exp],
                                                                     str(os.path.join(path_to_save_results,
                                                                                      'output_exp_{}_windows_{}_MAE.csv'.format(
                                                                                          results_head[exp],forecasting_horizon)))) + '-*-' * 10 + '\n')
        df_exp_country_results_MAE.to_csv(
            os.path.join(path_to_save_results, 'output_exp_{}_windows_{}_MAE.csv'.format(results_head[exp],forecasting_horizon)), index=False)

        ##Save this experiment MAPE results for all countries
        df_exp_country_results_MAPE.loc[:, results_head[exp]] = df_results_MAPE.loc[:, results_head[exp]].copy()
        print('-*-' * 10 + "Saving results for exp {} to {} ".format(results_head[exp],
                                                                     str(os.path.join(path_to_save_results,
                                                                                      'output_exp_{}_windows_{}_MAPE.csv'.format(
                                                                                          results_head[exp],forecasting_horizon)))) + '-*-' * 10 + '\n')
        df_exp_country_results_MAPE.to_csv(
            os.path.join(path_to_save_results, 'output_exp_{}_windows_{}_MAPE.csv'.format(results_head[exp], forecasting_horizon)), index=False)

        ##Save this experiment sMAPE results for all countries
        df_exp_country_results_sMAPE.loc[:, results_head[exp]] = df_results_sMAPE.loc[:, results_head[exp]].copy()
        print('-*-' * 10 + "Saving results for exp {} to {} ".format(results_head[exp],
                                                                     str(os.path.join(path_to_save_results,
                                                                                      'output_exp_{}_windows_{}_sMAPE.csv'.format(
                                                                                          results_head[exp], forecasting_horizon)))) + '-*-' * 10 + '\n')
        df_exp_country_results_sMAPE.to_csv(
            os.path.join(path_to_save_results, 'output_exp_{}_windows_{}_sMAPE.csv'.format(results_head[exp], forecasting_horizon)),
            index=False)

    ##Save all
    print('-*-' * 10 + "Saving all results to {} ".format(path_to_save_results) + '-*-' * 10 + '\n')
    df_results_RMSE.to_csv(os.path.join(path_to_save_results, 'Output_ALL_countries_RMSE_Windows_{}.csv'.format(forecasting_horizon)), index=False)
    df_results_AE.to_csv(os.path.join(path_to_save_results, 'Output_ALL_countries_AE_Windows_{}.csv'.format(forecasting_horizon)), index=False)
    df_results_MAE.to_csv(os.path.join(path_to_save_results, 'Output_ALL_countries_MAE_Windows_{}.csv'.format(forecasting_horizon)), index=False)
    df_results_MAPE.to_csv(os.path.join(path_to_save_results, 'Output_ALL_countries_MAPE_Windows_{}.csv'.format(forecasting_horizon)), index=False)
    df_results_sMAPE.to_csv(os.path.join(path_to_save_results, 'Output_ALL_countries_sMAPE_Windows_{}.csv'.format(forecasting_horizon)), index=False)
#!/bin/bash
echo "Starting training with TFT model"
python train_evaluate_models.py --model=TFT --forecasting_horizon=7 --n_epochs=20 --learning_rate=1e-3 --num_batches_per_epoch=100 --stock_target=Price | tee Logs_TFT_price.txt
echo "Finishing training with TFT model"